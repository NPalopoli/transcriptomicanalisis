# -*- coding: utf-8 -*-
"""
Created on Sat Oct 19 12:55:59 2019

@author: Manu
"""

import pandas as pd

import matplotlib.pyplot as plt

from matplotlib_venn import venn2


DEG_WT_1h = pd.read_csv("C:\\Users\\Manu\\Desktop\\TranscriptomicAnalisis\\Datos\\DEG_filter_1h.txt",sep="\t") #Se debe especificar ruta del archivo

DEG_WT_24h = pd.read_csv("C:\\Users\\Manu\\Desktop\\TranscriptomicAnalisis\\Datos\\DEG_filter_24h.txt",sep="\t")



DEG_genenames_1h = set(DEG_WT_1h["Unnamed: 0"])

DEG_genenames_24h = set(DEG_WT_24h["Unnamed: 0"])



plt.figure(figsize=(10,10))



plt.subplot(3,1,1) 

venn2([DEG_genenames_1h, DEG_genenames_24h], set_labels = ("Diferencial gens 1h", "Diferencial gens 24h"))

plt.title("Venn diagram for DEG in 24h of cold vs 1h of cold")



DEG_upregulated_1h = DEG_WT_1h[DEG_WT_1h.logFC > 0]
DEG_downregulated_1h = DEG_WT_1h[DEG_WT_1h.logFC < 0]

DEG_upregulated_24h = DEG_WT_24h[DEG_WT_24h.logFC > 0]
DEG_downregulated_24h = DEG_WT_24h[DEG_WT_24h.logFC < 0]

DEG_genenames_up_1h = set(DEG_upregulated_1h["Unnamed: 0"])
DEG_genenames_up_24h = set(DEG_upregulated_24h["Unnamed: 0"])



plt.subplot(3,1,2)

venn2([DEG_genenames_up_1h, DEG_genenames_up_24h], set_labels = ("Upregulated gens 1h", "Upregulated gens 24h"))

plt.title("Venn diagram for upregulated genes in 24h of cold vs 1h of cold")



DEG_genenames_down_1h = set(DEG_downregulated_1h["Unnamed: 0"])
DEG_genenames_down_24h = set(DEG_downregulated_24h["Unnamed: 0"])



plt.subplot(3,1,3)

venn2([DEG_genenames_down_1h, DEG_genenames_down_24h], set_labels = ("Downregulated gens 1h", "Downregulated gens 24h"))

plt.title("Venn diagram for downregulated genes in 24h of cold vs 1h of cold")



plt.tight_layout()



plt.savefig('Venn_diagrams.png', format='png', bbox_inches='tight', dpi=300)

plt.close()