# -*- coding: utf-8 -*-
"""
Created on Fri Oct 11 17:37:58 2019

@author: Manu
"""

import pandas as pd

DEG_WT_1h = pd.read_csv("C:\\Users\\Manu\\Desktop\\DEG_filter_1h.txt",sep="\t") #Se debe especificar ruta del archivo

DEG_WT_24h = pd.read_csv("C:\\Users\\Manu\\Desktop\\DEG_filter_24h.txt",sep="\t")



Only_logFC_1h = DEG_WT_1h[["Unnamed: 0", "logFC"]]

Only_logFC_24h = DEG_WT_24h[["Unnamed: 0", "logFC"]]



Only_logFC_1h["logFC_1h"] = Only_logFC_1h["logFC"]

Only_logFC_24h["logFC_24h"] = Only_logFC_24h["logFC"]



Only_logFC_1h = Only_logFC_1h[["Unnamed: 0", "logFC_1h"]]

Only_logFC_24h = Only_logFC_24h[["Unnamed: 0", "logFC_24h"]]



Merged_table_1vs24 = pd.merge(left=Only_logFC_1h, right=Only_logFC_24h, left_on="Unnamed: 0", right_on="Unnamed: 0",how="outer")
