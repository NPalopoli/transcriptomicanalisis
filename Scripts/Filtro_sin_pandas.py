# -*- coding: utf-8 -*-
"""
Created on Thu Oct 10 10:13:07 2019

@author: Manu
"""
import math
import time
import sys

lineasep = "-" * 92

print("{}\n\n{}Bienvenido a RNA seq filter\n\n{}\n\n".format(lineasep, " " * 32, lineasep))

time.sleep(2)

PATH = input("Inserte el PATH al archivo tabulado a filtrar (por favor utilizar {}".format(r"'\\'): "))

if PATH[-4:(len(PATH) + 1)] != ".txt":
    PATH = PATH + ".txt"

try:
    f = open(PATH, "r")
    f.close()
except:
    print("\n\n¡Ooops! Algo ha salido mal, el PATH especificado no es un archivo de texto.")
    sys.exit("No es una dirección a un archivo válido.")

time.sleep(2)

print("\n\n{}¡Gracias!\n\n{}\n\n".format(" " * 40, lineasep))

time.sleep(2)

nombre_archivo_a_guardar = input("Inserte nombre del archivo a crear con los datos filtrados: ") 

if nombre_archivo_a_guardar[-4:(len(PATH) + 1)] != ".txt":
    nombre_archivo_a_guardar = nombre_archivo_a_guardar + ".txt"

time.sleep(2)

PATH_guardar = input("\n\nInserte PATH donde guardar {} (por favor utilizar {}".format(nombre_archivo_a_guardar, r"'\\'): "))

if PATH_guardar[-2:-1] != "\\":
    PATH_guardar = PATH_guardar + "\\"
    
PATH_guardar = PATH_guardar + nombre_archivo_a_guardar

try:
    f = open(PATH_guardar, "w")
    f.close()
except:
    print("\n\n¡Ooops! La dirección provista no es correcta, compruebe si la dirección provista tiene doble barra invertida.")
    sys.exit("No es una dirección válida para guardar.")

time.sleep(2)

print("\n\n{}¡Gracias!\n\n{}\n\n".format(" " * 40, lineasep))

with open(PATH, "r") as f:

    keys_gen = f.readline()
    
    keys_gen = keys_gen.replace("\n", "")
    
    keys_gen = keys_gen.split("\t")
    
    keys_gen.remove("")
    
    lines_to_iter = f.readlines()
    
    total_progress = len(lines_to_iter)
    
    print("{}Extrayendo data a filtrar:\n".format(" " * 32))
    
    print("{}0/{} datos extraídos\n".format(" " * 35, total_progress))
    
    Datos_gens = {}
    
    for indice, lines in enumerate(lines_to_iter):
        
        print("{}{}/{} datos extraídos\n".format(" " * 35, indice, total_progress))
        
        lines = lines.replace("\n","")
        
        lines = lines.split("\t")
        
        lines[-1] = lines[-1].replace(",", ".")
        
        lines[-2] = lines[-2].replace(",", ".")
        
        lines[-2] = float(lines[-2])
        lines[-3] = float(lines[-3])
        lines[-4] = float(lines[-4])
        lines[-5] = float(lines[-5])
        lines[-6] = float(lines[-6])
        
        if "E" in lines[-1] or "e" in lines[-1]:
            
            if "E" in lines[-1]:
                local_list = lines[-1].split("E")
            elif "e" in lines[-1]:
                local_list = lines[-1].split("e")
            
            local_list[0] = float(local_list[0])
            local_list[1] = float(local_list[1])
                
            lines[-1] = local_list[0] * (10 ** (local_list[1])) 
        else:
            
            lines[-1] = float(lines[-1])
        
        Datos_gens[lines[0]] = {}
        
        for i in range(0, len(keys_gen)):
            Datos_gens[lines[0]][keys_gen[i]] = lines[i+1]

print("{0}{1}/{1} datos extraídos\n".format(" " * 35, total_progress))

print("{}¡Todos los datos extraídos exitosamente!\n\n{}\n\n".format(" " * 28,lineasep))

total_progress = len(Datos_gens)

print("{}Filtrando datos extraidos:\n".format(" " * 33))

Filtered_gens = {}

print("{}0/{} datos filtrados\n".format(" " * 35, total_progress))

contador = 0

for gen, valores in Datos_gens.items():
    
    contador = contador + 1
    
    print("{}{}/{} datos filtrados\n".format(" " * 35, contador, total_progress))
    
    if valores["fdr.gen"] < 0.05 and math.sqrt((valores["logFC"])**2) > 0.58 :
        
        Filtered_gens[gen] = valores

print("{}¡Todos los datos filtrados exitosamente!\n\n{}\n\n".format(" " * 28,lineasep))

total_progress = len(Filtered_gens)

print("{}Guardando los datos filtrados:\n".format(" " * 31))

time.sleep(2)

print("{}0/{} datos guardados\n".format(" " * 35, total_progress))

contador = 0

f = open(PATH_guardar, "w")
    
for keys in keys_gen:
    f.write("\t{}".format(keys))
        
    if keys == keys_gen[len(keys_gen) - 1]:
        f.write("\n")
    
for gen_names in Filtered_gens:
    
    contador = contador + 1
    
    print("{}{}/{} datos guardados\n".format(" " * 35, contador, total_progress))
    
    f.write(gen_names)
        
    for columnas,valores in Filtered_gens[gen_names].items():
            
        f.write("\t" + str(valores))
            
        if valores == Filtered_gens[gen_names][keys_gen[-1]]:
            f.write("\n")

f.close()

time.sleep(2)

print("{}¡Todos los datos guardados exitosamente!\n\n{}\n\n".format(" " * 28,lineasep))

time.sleep(2)

print("{}Se han filtrado {} de un total de {} datos brindados\n\n".format(" " * 22, len(Filtered_gens), len(Datos_gens)))

time.sleep(2)

print("Los datos se encuentran en la dirección '{}\n\n'".format(PATH_guardar))

print("{}\n\n{}¡Gracias por utilizar RNAseq filter!\n\n{}\n\n".format(lineasep, " " * 32, lineasep))