# -*- coding: utf-8 -*-
"""
Created on Sab Oct 26 13:07:34 2019

@author: Pablo_A
"""

import pandas as pd

#Lectura de las tablas 
filtrado_1h = pd.read_csv("DEG_filter_1h.txt",sep="\t")
filtrado_24h = pd.read_csv("DEG_filter_24h.txt",sep="\t")
anotacion = pd.read_csv("../ATH_GO_GOSLIM.txt", sep="\t", header= None, 
    names= ["locus_name", "TAIR_accession","object_name","relationship_type","GO_term","GO_ID","TAIR_Keyword_ID",
        "Aspect","GOslim_term","Evidence_code","Evidence_description","Evidence_with","Reference","Annotator","Date_annotated"])

#Cambio del nombre de las variables logFC y fdr.gen
filtrado_1h["fdr.gen_1h"] = filtrado_1h["fdr.gen"]
filtrado_1h["logFC_1h"] = filtrado_1h["logFC"]
filtrado_24h["fdr.gen_24h"] = filtrado_24h["fdr.gen"]
filtrado_24h["logFC_24h"] = filtrado_24h["logFC"]


#Union de las tablass 1h y 24h
merged_table_1vs24 = pd.merge(
left=filtrado_1h.drop(["logFC","fdr.gen"], axis=1), 
right=filtrado_24h.drop(["logFC","fdr.gen"], axis=1), 
left_on=["Unnamed: 0",'commName', 'coordGene', 'start', 'end', 'width','effWidth'], 
right_on=["Unnamed: 0",'commName', 'coordGene', 'start', 'end', 'width','effWidth'],
how="outer")

#Union de la tabla merge con la de anotacciones

result = pd.merge(merged_table_1vs24, anotacion, how='inner', left_on="Unnamed: 0", right_on= "locus_name")

#Guardado de la tabla con las anotaciones

result.to_csv("filtrada_anotada.txt",sep="\t",index=False)

