# Entrega de trabajo final del curso "Programación en Python para Biología Computacional"

### Autores: Aguilar, Pablo ; Czarnowski, Ian ; Da Ponte, Manuel ; Nogar, Bárbara
___
## ¿Sobre qué datos se trabajó?

El presente trabajo se realizó sobre un experimento realizado en la Fundación Instituto
Leloir. Este consistía en estudiar la expresión de genes en plantas de Arabidopsis thaliana sometidas a frío
por un tiempo breve (1 hora) frente a plantas sometidas a frío por un día entero. 

Los datos con que se contaba provenían de una secuenciación de RNA pair end, habiendo
generado las libraries por Illumina. Se contaba con una tabla que contenía los valores normalizados de la expresión de los genes
de los dos tratamientos ya relativizados a la expresión en las plantas no sometidas al frío.

El excel en donde se encontraban todos los datos se encuentra en la carpeta "Datos".
Ver [datos completos sin trabajar](Datos/Data_completa_excel.xlsx).

Cabe destacar que la tabla de excel poseía los datos crudos, como también la filtrada,
los genes diferencialmente expresados (DEG). Los criterios de filtro que utilizaron
los autores del experimento fueron los siguientes:
* Fdr < 0,05 (False Discovery Rate, es un p-valor corregido).

* Log2FC > |0,58| (Siendo FC el fold change).

## Scripts realizados

* ### Filtrado de los datos:

Si bien los investigadores proporcionaron los datos filtrados, se utilizaron los 
conocimientos adquiridos durante el curso para reproducir el filtrado. Se trabajo
sobre las tablas pasadas a archivos de texto, separándolas según tratamiento (Estos 
datos crudos se pueden encontrar en la carpeta "Datos"). Ver [datos crudos frío 1 hora](Datos/Raw_data_WT_1h.txt)
 y [datos crudos frío 24 horas](Datos/Raw_data_WT_24h.txt).
 
En primer lugar se realizó el filtrado de los datos sin utilizar ninguna librería 
para afianzar los conceptos vistos durante la clase de Python. Ver [filtrado sin pandas](Scripts/Filtro_sin_pandas.py).
Los resultados de dicho script dieron iguales a los proporcionados por los investigadores.

Luego, se realizó la misma filtración, pero esta vez haciendo uso de la librería 
pandas de Python, comparando de esta manera el poder de esta. Ver [filtrado con pandas](Scripts/Filtro_pandas.py).
Al igual que sin pandas, los resultados fueron los mismos.

Las tablas con los datos filtrados se pueden encontrar en la carpeta datos. Ver [datos filtrados frío 1 hora](Datos/DEG_filter_1h.txt) y
 [datos filtrados frío 24 horas](Datos/DEG_filter_24h.txt).
 
* ### Diagrama de Venn

Antes de realizar este gráfico se hizo un merge de las dos tablas filtradas de los
dos tratamientos por medio de un [script](Scripts/Mergeo_tablas.py).

Luego se efectuó diagramas de Venn que muestren que genes se expresaron diferencialmente
en los dos tratamientos y cuales no, y lo mismo sólo teniendo en cuenta los genes 
upregulated y downregulated. Ver [script diagrama de Venn](Scripts/Venn_diagrams_v0.2.py) y 
[resultado de diagrama de Venn](Figuras/Venn_diagrams.png).

* ### Vulcan plot

Se realizó además un gráfico del logarirmo de fold change versus el logaritmo del
fdr para los dos tratamientos, para observar diferencias de expresión. Ver [script digrama de Vulcan](Scripts/Vulcan_plot.py)
 y [resultado diagrama de vulcan](Figuras/volcano.png).
 
* ### Expresión según función molecular

Luego se quizo entender cuales funciones moleculares eran las que contenian mayor numero de genes diferencialmente expresados sin importar
 el tratamiento. Para esto se debió descargar la ontología génica de la arabidopsis, 
 planta que fue utilizada para este experimento (Ver (ontogenia génica de arabidopsis)[Datos/ATH_GO_GOSLIM.txt.gz] 
 y (clave de las colmunas de ontogenia)[Datos/Columnas_ATH_GO_GOSLIM.txt]). Para después agregar
 la ontogenia a la tabla (Ver [nuevo mergeo de tabla](Datos/filtrada_anotada.txt) y (merge script)[Scripts/Merge_Anotacion.py]).
 Con esta nueva tabla se realizó el gráfico del número de genes diferencialmente expresados
según su función molecular (Ver [gráfico obtenido](Figuras/molecularf.png) y su [script para realizarlo](Scripts/Anotaciones_funcionales.py)).

* ### Heatmap

Se hizo un heatmap clusterizado, utilizando los paquetes seaborn y matplotlib de python, 
para poder comparar rápidamente el patrón de expresión de los genes diferencialmente expresados entre los dos
 tratamientos. Ver [script para realizar el heatmap](Scripts/Heatmap.py) y [resultado del heatmap](Figuras/Heatmap_DEG1hvs24hs.png).
